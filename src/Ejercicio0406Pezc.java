/**
 * Fichero: Ejercicio0509.java
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 26-nov-2013
 */
public class Ejercicio0406Pezc implements Cloneable { // Pezc

  private static int numpeces = 0;
  protected String nombre;

  public String getNombre() {
    return this.nombre;
  }

  public void setNombre(String s) {
    this.nombre = s;
  }

  public Object clone() {
    Object objeto = null;
    try {
      objeto = super.clone();
      numpeces++;
    } catch (CloneNotSupportedException ex) {
      System.out.println(" Error al duplicar");
    }
    return objeto;
  }

  public boolean equals(Ejercicio0406Pezc ese) {
    if (ese.getNombre() == this.getNombre()) {
      return true;
    }
    return false;
  }

  public int getpeces() {
    return this.numpeces;
  }

  Ejercicio0406Pezc() {
    numpeces++;
  }

  public static void main(String[] args) {
    Ejercicio0406Pezc p1 = new Ejercicio0406Pezc();
    p1.setNombre("Gulli");
    Ejercicio0406Pezc p2 = new Ejercicio0406Pezc();
    p2.setNombre("Escalar");
    Ejercicio0406Pezc p3 = (Ejercicio0406Pezc) p2.clone();
    System.out.println(p1.getNombre());
    System.out.println(p2.getNombre());
    System.out.println(p3.getNombre());
    System.out.println(p3.equals(p2));
    System.out.println(p3.getpeces());
  }
}
/* EJECUCION:
Gulli
Escalar
Escalar
false
3
*/
