/**
 * Fichero: Ejercicio0601.java
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 16-dic-2013
 */
public class Ejercicio0412Bebe1 {

  static void pedir() {
    System.out.println(str1 + " , " + str2 + " , " + str3);
  }

  static {
    str2 = "mama pipi";
    str3 = "mama agua";
  }

  Ejercicio0412Bebe1() {
    System.out.println("nacimiento del bebe");
  }
  static String str2, str3, str1 = "papa tengo caca";

  public static void main(String[] args) {
    System.out.println("El bebe se ha despertado y va a pedir cosas");
    System.out.println("El bebe dice: " + Ejercicio0412Bebe1.str1);
    Ejercicio0412Bebe1.pedir();
  }
  static Ejercicio0412Bebe1 b1 = new Ejercicio0412Bebe1();
  static Ejercicio0412Bebe1 b2 = new Ejercicio0412Bebe1();
  static Ejercicio0412Bebe1 b3 = new Ejercicio0412Bebe1();
}
