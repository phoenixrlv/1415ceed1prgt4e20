/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Ejercicio0410Interface;

/**
 * Fichero: BaseDatos.java
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 28-nov-2013
 */
public interface BaseDatos {

  void close();

  void open();
}
