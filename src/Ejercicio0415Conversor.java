/**
 * Fichero: Ejercicio0604.java
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 16-dic-2013
 */
public class Ejercicio0415Conversor {

  private int dato;

  Ejercicio0415Conversor(int p) {
    dato = p;
  }

  public String getNumero(String param) {
    if (param == "B") {
      return Integer.toBinaryString(dato);
    }
    if (param == "H") {
      return Integer.toHexString(dato);
    }
    if (param == "O") {
      return Integer.toOctalString(dato);
    }
    return "Parametro no reconocido";
  }

  ;

  public static void main(String[] args) {
    Ejercicio0415Conversor s = new Ejercicio0415Conversor(20);
    System.out.println(s.dato);
    System.out.println(s.getNumero("B"));
    System.out.println(s.getNumero("H"));
    System.out.println(s.getNumero("O"));
  }
}

/* EJECUCION:
 20
 10100
 14
 24
 */
