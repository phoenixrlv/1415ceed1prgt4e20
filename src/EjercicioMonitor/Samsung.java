/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package EjercicioMonitor;

/**
 * @date 19-nov-2014
 * Fichero Samsung.java
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 */
public class Samsung implements Monitor{

    @Override
    public void encender() {
        System.out.println("Encendido monitor Samsung");
    }
   

}
