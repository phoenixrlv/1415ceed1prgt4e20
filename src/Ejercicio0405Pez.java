/**
 * Fichero: Ejercicio0508.java
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 26-nov-2013
 */
public class Ejercicio0405Pez { // Pez

// Miembros
  private String nombre;

// Metodos
  public String getNombre() {
    return this.nombre;
  }

  public void setNombre(String s) {
    this.nombre = s;
  }

  public Ejercicio0405Pez clone() {
    Ejercicio0405Pez c = new Ejercicio0405Pez();
    c.nombre = this.nombre;
    return c;
  }

  public boolean equals(Ejercicio0405Pez p) {
    if (this.nombre == p.nombre) {
      return true;
    } else {
      return false;
    }
  }

  public static void main(String[] args) {
    Ejercicio0405Pez p1 = new Ejercicio0405Pez();
    Ejercicio0405Pez p2 = new Ejercicio0405Pez();
    Ejercicio0405Pez p3 = new Ejercicio0405Pez();

    p1.setNombre("Payaso");
    p2.setNombre("Espada");
    p3 = p2.clone();

    System.out.println(p1.getNombre());
    System.out.println(p2.getNombre());
    System.out.println(p3.getNombre());

    if (p2.equals(p3)) {
      System.out.println("Iguales");
    }

  }

}
/* Ejecucion
 Payaso
 Espada
 Espada
 Iguales
 */
