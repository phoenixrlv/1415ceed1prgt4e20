package Ejercicio0404;

import ejercicio0507.utilidades.mates.Potencia;
import ejercicio0507.utilidades.mates.Suma;

/**
 * Fichero: Ejercicio0507.java
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 */
public class Ejercicio0507 {

  public static void main(String args[]) {
    Suma s = new Suma();
    Potencia p = new Potencia();
    System.out.println(s.sumar(2, 3));
    System.out.println(p.potencia(2, 3));

  }
}
